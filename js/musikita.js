/**
 * Created by jorge on 24/02/16.
 */

    //<![CDATA[
$(document).ready(function () {

    // show data
    var today = new Date();
    var timeNow = today.toLocaleTimeString() + " -  " + today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear();
    $('#data').append(timeNow);// la ora y fecha  en formato 8:54:50 AM - 24/2/2016


    // to call api
    $.getJSON("api-music.php", recorrido);
    function recorrido(data) {


        // print template lista-indice dentro de #contenedor pasandole data
        origen = document.getElementById('lista-indice').innerHTML;
        plantilla = Handlebars.compile(origen);
        $('#contenedor').html(plantilla(data));


        // usamos Jplayer
        var myPlaylist = new jPlayerPlaylist({
                jPlayer: "#jquery_jplayer_1",
                cssSelectorAncestor: "#jp_container_1"
            },
            data.musikita.mejores.music,
            {

                playlistOptions: {
                    autoPlay: true,
                    loopOnPrevious: false

                },
                swfPath: "jplayer",
                supplied: "webmv, ogv, m4v, oga, mp3",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: true,
                audioFullScreen: true


            });


        var arr = [];
        $.each(data.musikita, function (i) {
            arr.push(i);
        });
        console.log(arr);
        //for (var i = 0; i < arr.length; i++) {
        //    console.log(arr[i]);
        //
        //}

        myPlaylist.shuffle(true);
        myPlaylist.option("autoPlay", true);
        myPlaylist.option("enableRemoveControls", true);

        // lista de categorias cargadas con setPlaylist
        $("#musikex").click(function () {
            myPlaylist.setPlaylist(data.musikita.mejores.music);
        });


        $("#bachata").click(function () {
            myPlaylist.setPlaylist(data.musikita.bachata.music);
        });

        $("#chicha").click(function () {
            myPlaylist.setPlaylist(data.musikita.chicha.music);
        });


        $("#cumbia").click(function () {
            myPlaylist.setPlaylist(data.musikita.cumbia.music);
        });

        $("#cumbiaantologia").click(function () {
            myPlaylist.setPlaylist(data.musikita.cumbiaantologia.music);
        });

        $("#disco").click(function () {
            myPlaylist.setPlaylist(data.musikita.disco.music);
        });

        $("#italiano").click(function () {
            myPlaylist.setPlaylist(data.musikita.italiano.music);
        });

        $("#mejores").click(function () {
            myPlaylist.setPlaylist(data.musikita.mejores.music);
        });

        $("#movidita").click(function () {
            myPlaylist.setPlaylist(data.musikita.movidita.music);
        });

        $("#reggeton").click(function () {
            myPlaylist.setPlaylist(data.musikita.reggeton.music);
        });

        $("#rocas").click(function () {
            myPlaylist.setPlaylist(data.musikita.rocas.music);
        });

        $("#rocktono").click(function () {
            myPlaylist.setPlaylist(data.musikita.rocktono.music);
        });

        $("#salsa").click(function () {
            myPlaylist.setPlaylist(data.musikita.salsa.music);
        });

        $("#salsatonear").click(function () {
            myPlaylist.setPlaylist(data.musikita.salsatonear.music);
        });

        $("#seisvoltios").click(function () {
            myPlaylist.setPlaylist(data.musikita.seisvoltios.music);
        });

        $("#techno").click(function () {
            myPlaylist.setPlaylist(data.musikita.techno.music);
        });

        $("#villera").click(function () {
            myPlaylist.setPlaylist(data.musikita.villera.music);
        });


    }

});
//]]>
